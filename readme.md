
------------
Array[1]
Vigencia: 01.01.2019
Valor Mensal: R$ 998,00
Valor Diario: R$ 33,27
Valor Hora: R$ 4,54
Norma Legal: Decreto 9.661/2019
D.O.U.: 01.01.2019
------------
Array[2]
Vigencia: 01.01.2018
Valor Mensal: R$ 954,00
Valor Diario: R$ 31,80
Valor Hora: R$ 4,34
Norma Legal: Decreto 9.255/2017
D.O.U.: 29.12.2017
------------
Array[3]
Vigencia: 01.01.2017
Valor Mensal: R$ 937,00
Valor Diario: R$ 31,23
Valor Hora: R$ 4,26
Norma Legal: Decreto 8.948/2016
D.O.U.: 30.12.2016
------------
Array[4]
Vigencia: 01.01.2016
Valor Mensal: R$ 880,00
Valor Diario: R$ 29,33
Valor Hora: R$ 4,00
Norma Legal: Decreto 8.618/2015
D.O.U.: 30.12.2015
------------
Array[5]
Vigencia: 01.01.2015
Valor Mensal: R$ 788,00
Valor Diario: R$ 26,27
Valor Hora: R$ 3,58
Norma Legal: Decreto 8.381/2014
D.O.U.: 30.12.2014
------------
Array[6]
Vigencia: 01.01.2014
Valor Mensal: R$ 724,00
Valor Diario: R$ 24,13
Valor Hora: R$ 3,29
Norma Legal: Decreto 8.166/2013
D.O.U.: 24.12.2013
------------
Array[7]
Vigencia: 01.01.2013
Valor Mensal: R$ 678,00
Valor Diario: R$ 22,60
Valor Hora: R$ 3,08
Norma Legal: Decreto 7.872/2012
D.O.U.: 26.12.2012
------------
Array[8]
Vigencia: 01.01.2012
Valor Mensal: R$ 622,00
Valor Diario: R$ 20,73
Valor Hora: R$ 2,83
Norma Legal: Decreto 7.655/2011
D.O.U.: 26.12.2011
------------
Array[9]
Vigencia: 01.03.2011
Valor Mensal: R$ 545,00
Valor Diario: R$ 18,17
Valor Hora: R$ 2,48
Norma Legal: Lei 12.382/2011
D.O.U.: 28.02.2011
------------
Array[10]
Vigencia: 01.01.2011
Valor Mensal: R$ 540,00
Valor Diario: R$ 18,00
Valor Hora: R$ 2,45
Norma Legal: MP 516/2010
D.O.U.: 31.12.2010
------------
Array[11]
Vigencia: 01.01.2010
Valor Mensal: R$ 510,00
Valor Diario: R$ 17,00
Valor Hora: R$ 2,32
Norma Legal: Lei 12.255/2010
D.O.U.: 16.06.2010
------------
Array[12]
Vigencia: 01.02.2009
Valor Mensal: R$ 465,00
Valor Diario: R$ 15,50
Valor Hora: R$ 2,11
Norma Legal: Lei 11.944/2009
D.O.U.: 29.05.2009
------------
Array[13]
Vigencia: 01.03.2008
Valor Mensal: R$ 415,00
Valor Diario: R$ 13,83
Valor Hora: R$ 1,89
Norma Legal: Lei 11.709/2008
D.O.U.: 20.06.2008
------------
Array[14]
Vigencia: 01.04.2007
Valor Mensal: R$ 380,00
Valor Diario: R$ 12,67
Valor Hora: R$ 1,73
Norma Legal: Lei 11.498/2007
D.O.U.: 29.06.2007
------------
Array[15]
Vigencia: 01.04.2006
Valor Mensal: R$ 350,00
Valor Diario: R$ 11,67
Valor Hora: R$ 1,59
Norma Legal: MP 288/2006
D.O.U.: 31.03.2006
------------
Array[16]
Vigencia: 01.05.2005
Valor Mensal: R$ 300,00
Valor Diario: R$ 10,00
Valor Hora: R$ 1,36
Norma Legal: Lei 11.164/2005
D.O.U.: 22.04.2005
------------
Array[17]
Vigencia: 01.05.2004
Valor Mensal: R$ 260,00
Valor Diario: R$ 8,67
Valor Hora: R$ 1,18
Norma Legal: MP 182/2004
D.O.U.: 30.04.2004
------------
Array[18]
Vigencia: 01.04.2003
Valor Mensal: R$ 240,00
Valor Diario: R$ 8,00
Valor Hora: R$ 1,09
Norma Legal: MP 116/2003
D.O.U.: 03.04.2003
------------
Array[19]
Vigencia: 01.04.2002
Valor Mensal: R$ 200,00
Valor Diario: R$ 6,67
Valor Hora: R$ 0,91
Norma Legal: MP 35/2002
D.O.U.: 28.03.2002
------------
Array[20]
Vigencia: 01.04.2001
Valor Mensal: R$ 180,00
Valor Diario: R$ 6,00
Valor Hora: R$ 0,82
Norma Legal: MP 2.142/2001 (atual 2.194-5)
D.O.U.: 30.03.2001
------------
Array[21]
Vigencia: 03.04.2000
Valor Mensal: R$ 151,00
Valor Diario: R$ 5,03
Valor Hora: R$ 0,69
Norma Legal: Lei 9.971/2000
D.O.U.: 24.03.2000
------------
FIM DO PROCESSAMENTO