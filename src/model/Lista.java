package model;

public class Lista {
    private String vigencia;
    private String vrMensal;
    private String vrDiario;
    private String vrHora;
    private String nrLegal;
    private String sDou;
    
    public Lista(){
    }

    public Lista(String vigencia, String vrMensal, String vrDiario, String vrHora, String nrLegal, String sDou) {
        this.vigencia = vigencia;
        this.vrMensal = vrMensal;
        this.vrDiario = vrDiario;
        this.vrHora = vrHora;
        this.nrLegal = nrLegal;
        this.sDou = sDou;
    }

    public String getVigencia() {
        return vigencia;
    }

    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    public String getVrMensal() {
        return vrMensal;
    }

    public void setVrMensal(String vrMensal) {
        this.vrMensal = vrMensal;
    }

    public String getVrDiario() {
        return vrDiario;
    }

    public void setVrDiario(String vrDiario) {
        this.vrDiario = vrDiario;
    }

    public String getVrHora() {
        return vrHora;
    }

    public void setVrHora(String vrHora) {
        this.vrHora = vrHora;
    }

    public String getNrLegal() {
        return nrLegal;
    }

    public void setNrLegal(String nrLegal) {
        this.nrLegal = nrLegal;
    }

    public String getsDou() {
        return sDou;
    }

    public void setsDou(String sDou) {
        this.sDou = sDou;
    }

    @Override
    public String toString() {
        return "Lista{" + "vigencia=" + vigencia + ", vrMensal=" + vrMensal + ", vrDiario=" + vrDiario + ", vrHora=" + vrHora + ", nrLegal=" + nrLegal + ", sDou=" + sDou + '}';
    }
}