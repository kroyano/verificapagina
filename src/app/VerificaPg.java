package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

 /*
 * @author Rafael Maciel Pereira Silva
 * @version 1.0
 * @since 10/09/2019
 */
public class VerificaPg extends Application {
    
    public static boolean start;
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/view/FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setTitle("Verifica Pagina");
        stage.show();
        stage.setOnCloseRequest(e -> System.exit(0));
        
    }

    public static void main(String[] args) {
        launch(args);
        start = false;
    }
}