package verifica;

import app.VerificaPg;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import model.Lista;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author Rafael Maciel P. Silva
 * @version 1.0
 * @since 10/09/2019
 */
public class FXMLDocumentController implements Initializable {

    private WebDriver driver;
    private static ArrayList<Lista> lista = new ArrayList();

    private Thread thrdVerifica = null;
    private boolean iniciado = false;

    @FXML
    private TextArea txtStatus;

    @FXML
    private void acessaSite(ActionEvent event) {

        //Abre o navegador e acesso site
        try {
            //Abrir Chrome
            driver = new ChromeDriver();
            driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            driver.get("http://www.guiatrabalhista.com.br/guia/salario_minimo.htm");
        } catch (WebDriverException ex) {
        }
    }

    @FXML
    private void liberaNavegador(ActionEvent event) {
        //Fecha o navegador
        try {
            driver.close();
            driver.quit();
        } catch (NoSuchWindowException ex) {
        }
    }

    @FXML
    private void iniciaVerificacao(ActionEvent event) {
        if (!VerificaPg.start) {
            VerificaPg.start = true;
            iniciado = true;
            iniciaThread();
        } else {
            VerificaPg.start = false;
            iniciado = false;
        }
    }

    private void iniciaThread() {

        if (VerificaPg.start) {
            if (thrdVerifica == null) {
                thrdVerifica = new ThreadVerifica();
            }
            //Verifica se a thread esta ativa, ou seja, se foi iniciada e ainda nao terminou...
            if (thrdVerifica.isAlive() == false) {
                thrdVerifica.start();
            }
        }
    }

    private class ThreadVerifica extends Thread {

        private int obtemDadosPagina() {
            int e, ret = 0;
            List<WebElement> table = null;
            List<WebElement> linhas = null;
            String vigencia, vrMensal, vrDiario;
            String vrHora, nrLegal, sDou;

            try {
                table = driver.findElements(By.xpath("//*[@id='content']/div[1]/table/tbody"));
                linhas = table.get(0).findElements(By.xpath(".//tr"));
            } catch (NoSuchElementException ex) {
                ret = 1;
            } catch (WebDriverException ex) {
                ret = 1;
            }
            
            if (linhas != null) {
                try {
                    for (e = 1; e < linhas.size(); e++) {
                        //Obtenho os dados de cada coluna
                        vigencia = linhas.get(e).findElement(By.xpath(".//td[1]")).getText().trim();
                        vrMensal = linhas.get(e).findElement(By.xpath(".//td[2]")).getText().trim();
                        vrDiario = linhas.get(e).findElement(By.xpath(".//td[3]")).getText().trim();
                        vrHora = linhas.get(e).findElement(By.xpath(".//td[4]")).getText().trim();
                        nrLegal = linhas.get(e).findElement(By.xpath(".//td[5]")).getText().trim();
                        sDou = linhas.get(e).findElement(By.xpath(".//td[6]")).getText().trim();

                        //Adiciono os dados da linha no Array
                        addLista(vigencia, vrMensal, vrDiario, vrHora, nrLegal, sDou);

                        //Imprimo o resultado
                        String sDados = "\n------------\n";
                        sDados = sDados + "Array[" + e + "]\nVigencia: " + vigencia + "\nValor Mensal: " + vrMensal + "\nValor Diario: "
                                + vrDiario + "\nValor Hora: " + vrHora + "\nNorma Legal: " + nrLegal + "\nD.O.U.: " + sDou;
                        txtStatus.appendText(sDados);
                    }
                } catch (NoSuchElementException ex) {
                } catch (WebDriverException ex) {
                    System.out.println(ex);
                }
            }
            txtStatus.appendText("\n------------\nFIM DO PROCESSAMENTO");
            table = null;
            linhas = null;

            return (ret);
        }

        //Método que adiciona os itens no Array 
        public void addLista(String vigencia, String vrMensal, String vrDiario, String vrHora, String nrLegal, String sDou) {
            Lista list = new Lista();
            list.setVigencia(vigencia);
            list.setVrMensal(vrMensal);
            list.setVrDiario(vrDiario);
            list.setVrHora(vrHora);
            list.setNrLegal(nrLegal);
            list.setsDou(sDou);
            lista.add(list);
        }

        @Override
        public void run() {
            iniciado = false;

            thrdVerifica.setPriority(Thread.NORM_PRIORITY);
            try {
                iniciado = VerificaPg.start;
            } catch (Exception ex) {
                iniciado = false;
            }
            if (iniciado == true) {
                obtemDadosPagina();
            } else {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}